#include "pid.h"
#include <stdio.h>
#include <ctime>
#include <cstdlib>

int main() {

    PID pid = PID(0.1, 100, -100, 0.5, 0.06, 0.01);

    //随机种子
    srand((int)time(NULL));

    double val = 10.234;
    for (int i = 0; i < 10000; i++) {
        double inc = pid.calculate(80.515, val);

        //生成随机误差
        double err = (rand()%10);
        err = err/1000;

        val += inc;
        val += err;

        printf("counter:%d  val:% 7.3f inc:% 7.4f err:%7.3f\n", i, val, inc, err);

        double tmp = 0;
        tmp = val - 80.515;
        if((tmp <= 0.001) && (tmp >= -0.001))
        {
            break;
        }
    }

    return 0;
}
